local PART={}
PART.ID = "dtcapdrsw"
PART.Name = "dtcapdrsw"
PART.Model = "models/coreylz/smith-capaldi/console12_detail.mdl"
PART.AutoSetup = true
PART.Animate = true

if SERVER then
	function PART:Use(activator)
		self:SetUseType( SIMPLE_USE )
		self:EmitSound( "tardis/capaldi/Handbrake.wav" )
		self.exterior:ToggleDoor()
	end
end




TARDIS:AddPart(PART,e)