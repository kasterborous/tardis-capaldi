local PART={}
PART.ID = "dtcaptrcr"
PART.Name = "dtcaptrcr"
PART.Model = "models/coreylz/smith-capaldi/tracers.mdl"
PART.AutoSetup = true
PART.Collision = true

if CLIENT then
	function PART:Think()
		local ext=self.exterior
                mat=Material("coreylz/smith-capaldi/Tracer Light")
		if ext:GetData("flight") or ext:GetData("teleport") or ext:GetData("vortex") then
			  self:SetMaterial("coreylz/smith-capaldi/Tracer Light Anim")
                else
                          self:SetMaterial("coreylz/smith-capaldi/Tracer Light")
		end
	end
end

TARDIS:AddPart(PART,e)