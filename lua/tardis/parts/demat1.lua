local PART={}
PART.ID = "dtcapdemat1"
PART.Name = "dtcapdemat1"
PART.Model = "models/coreylz/smith-capaldi/flightlever.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true

if SERVER then
	function PART:Initialize()
		self:SetColor(Color(255,255,255,255))
	end
   
	function PART:Use(activator)
		self:EmitSound("tardis/capaldi/lever.wav")
		local ext=self.exterior
		if ext:GetData("vortex") then ext:Mat() else ext:Demat() end
	end
end
 
TARDIS:AddPart(PART,e)