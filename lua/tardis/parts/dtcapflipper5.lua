local PART={}
PART.ID = "dtcapflipper5"
PART.Name = "dtcapflipper5"
PART.Model = "models/coreylz/smith-capaldi/Flipper.mdl"
PART.AutoSetup = true

PART.Animate = true
if SERVER then
	function PART:Initialize()
		self:SetColor(Color(255,255,255,255))
	end

	function PART:Use()
		self:EmitSound( "tardis/capaldi/Handbrake.wav")
	end
end

TARDIS:AddPart(PART,e)